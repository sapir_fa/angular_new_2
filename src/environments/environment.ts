// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyB6oJMUNxUS7Q0eU19KmrtFgkE9fgj7vlY",
    authDomain: "examp-jce.firebaseapp.com",
    projectId: "examp-jce",
    storageBucket: "examp-jce.appspot.com",
    messagingSenderId: "825845351081",
    appId: "1:825845351081:web:bb2e138370ab7d7103dfb8"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
