import { Book } from './../interfaces/book';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
//import * as EventEmitter from 'events';

@Component({
  selector: 'app-book-form',
  templateUrl: './book-form.component.html',
  styleUrls: ['./book-form.component.css']
})
export class BookFormComponent implements OnInit {

  constructor() { }

  @Input() title: string;
  @Input() author: string;
  @Input() id: string;
  @Output() update = new EventEmitter<Book>()
  @Output() closeEdit = new EventEmitter<null>()
  
  updateParent() {
    let book:Book = {id:this.id, title:this.title, author:this.author};
    this.update.emit(book);
  }
  tellParentToClose(){
    this.closeEdit.emit();
  }

  ngOnInit(): void {
  }
}
