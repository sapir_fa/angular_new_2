import { User } from './interfaces/user';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { map } from 'rxjs/operators';
import { title } from 'process';

@Injectable({
  providedIn: 'root'
})
export class BooksService {
  // books = [{title:'Alice in Wonderland', author:'Lewis Carrol', summary:'blabla1'},
  // {title:'War and Peace', author:'Leo Tolstoy', summary:'blabla2'},
  // {title:'The Magic Mountain', author:'Thomas Mann', summary:'blabla3'}]

  // public addBooks(){
  //   setInterval(()=>this.books.push({title:'A new one',author:'New author',summary:'Short sammary'}),2000)
  // }

  // public getBooks(){
  //   const booksObservable = new Observable(observer => {
  //     setInterval(()=>observer.next(this.books),500)
  //   });
  //   return booksObservable;
  // } 
  constructor(private db: AngularFirestore) { }
  
  bookCollection: AngularFirestoreCollection;
  UserCollection: AngularFirestoreCollection =this.db.collection('users');

  public getBooks(userId) {
    this.bookCollection = this.db.collection(`users/${userId}/books`,
    ref=> ref.orderBy('title','asc').limit(4))
    return this.bookCollection.snapshotChanges()
  }

  public getPrevBooks(userId, startAt) {
    this.bookCollection = this.db.collection(`users/${userId}/books`,
    ref=> ref.orderBy('title','asc').limit(4).startAt(startAt));
    return this.bookCollection.snapshotChanges()
  }

  public nextPage(userId,startAfter): Observable<any[]>{
    this.bookCollection = this.db.collection(`users/${userId}/books`, 
    ref => ref.limit(4).orderBy('title', 'asc')
      .startAfter(startAfter))    
    return this.bookCollection.snapshotChanges();
  }
  




  deleteBook(Userid:string,id:string){
    this.db.doc(`users/${Userid}/books/${id}`).delete();
  }
  
  addBook(userId:string, title:string, author:string){
    const book = {title, author};
    this.UserCollection.doc(userId).collection('books').add(book);
    }

  updateBook(userId:string, id:string, title:string, author:string){
    this.db.doc(`users/${userId}/books/${id}`).update(
      {
        title:title,
        author:author
      }
    )
  }
}
