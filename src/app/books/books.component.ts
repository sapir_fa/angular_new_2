import { Book } from './../interfaces/book';
import { AuthService } from './../auth.service';
import { BooksService } from './../books.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'books',
  templateUrl: './books.component.html',
  styleUrls: ['./books.component.css']
})
export class BooksComponent implements OnInit {

  books: Book[];
  books$;
  userId: string;
  bookpanelOpenState = false;
  editstate = [];
  addstate: boolean = false;
  //Save first document in snapshot of items received
  firstDocumentArrived: any;

  //Save last document in snapshot of items received
  lastDocumentArrived: any;

  //Keep the array of first document of previous pages
  prev_strt_at: any[] = [];

  push_prev_startAt(prev_first_doc) {
    this.prev_strt_at.push(prev_first_doc);
  }

  remove_last_from_start_at() {
    this.prev_strt_at.splice(this.prev_strt_at.length - 1, 1);
  }
 
  get_prev_startAt(){
    return this.prev_strt_at[this.prev_strt_at.length - 1];
}
  constructor(private BooksService: BooksService, public authService: AuthService) { }

  ngOnInit(): void {
    this.authService.getUser().subscribe(
      user => {
        this.userId = user.uid;
        this.books$ = this.BooksService.getBooks(this.userId);
        this.books$.subscribe(
          docs => {
            this.lastDocumentArrived = docs[docs.length-1].payload.doc;
            this.firstDocumentArrived = docs[0].payload.doc;
            this.push_prev_startAt(this.firstDocumentArrived);  
            this.books = [];
            for (let document of docs) {
              const book: Book = document.payload.doc.data();
              book.id = document.payload.doc.id;
              this.books.push(book);
            }
          }
        )
      }
    )
  }

  nextPage() {
    console.log("nexttt")
    this.books$ = this.BooksService.nextPage(this.userId, this.lastDocumentArrived);
    this.books$.subscribe(
      docs => {
        this.lastDocumentArrived = docs[docs.length - 1].payload.doc;
        this.firstDocumentArrived = docs[0].payload.doc;
        this.push_prev_startAt(this.firstDocumentArrived);
        this.books = [];
        for (let document of docs) {
          const book: Book = document.payload.doc.data();
          book.id = document.payload.doc.id;
          this.books.push(book);
        }
        console.log(this.books)
      }
    )
  }
  




// this.BooksService.addBooks();
// this.books$ = this.BooksService.getBooks();
// this.books$.subscribe(books => this.books = books);

// .pipe(map(
//   collection => collection.map(
//     document => {
//       const data = document.payload.doc.data();
//       data.id = document.payload.doc.id;
//       return data;
//     }
//   )
// ))

previusPage(){
  this.remove_last_from_start_at()
  this.books$ = this.BooksService.getPrevBooks(this.userId, this.get_prev_startAt());
  this.books$.subscribe(
    docs => {
      this.lastDocumentArrived = docs[docs.length - 1].payload.doc;
      this.firstDocumentArrived = docs[0].payload.doc;
      this.books = [];
      for (let document of docs) {
        const book: Book = document.payload.doc.data();
        book.id = document.payload.doc.id;
        this.books.push(book);
      }
    }
  )
}


onDelete(bookId: string){
  this.BooksService.deleteBook(this.userId, bookId);
}

update(book: Book){
  this.BooksService.updateBook(this.userId, book.id, book.title, book.author);
}

add(book: Book){
  this.BooksService.addBook(this.userId, book.title, book.author);

}
}
