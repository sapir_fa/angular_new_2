import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Book } from '../interfaces/book';

@Component({
  selector: 'app-add-book',
  templateUrl: './add-book.component.html',
  styleUrls: ['./add-book.component.css']
})
export class AddBookComponent implements OnInit {

  
  constructor() { }

  @Input() title: string;
  @Input() author: string;
  @Output() update = new EventEmitter()
  @Output() closeEdit = new EventEmitter<null>()
  
  updateParent() {
    let book= {title:this.title, author:this.author};
    this.update.emit(book);
    this.title=null;
    this.author=null;
  }
  tellParentToClose(){
    this.closeEdit.emit();
  }

  ngOnInit(): void {
    console.log(this.title)
  }
}
