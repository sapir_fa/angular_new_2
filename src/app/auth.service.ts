import { User } from './interfaces/user';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';
import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';



@Injectable({
  providedIn: 'root'
})
export class AuthService {
  [x: string]: any;

user: Observable <User | null>


  login (email: string, password:string){
    return  this.afAuth.signInWithEmailAndPassword(email,password);
        }

  register (email: string, password:string){
    return this.afAuth.createUserWithEmailAndPassword(email,password);
  }

  getUser():Observable<User | null> {
    return this.user;
  }

  logout(){
    this.afAuth.signOut();
  }

  constructor(private afAuth:AngularFireAuth, private router:Router) { 
    this.user = this.afAuth.authState;
  }
}
