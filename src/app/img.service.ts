import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ImgService {

  private path:string = 'https://firebasestorage.googleapis.com/v0/b/examp-jce.appspot.com/o/'; 
  public images:string[] = [];
  constructor() {
    this.images[0] = this.path + 'ANFULARPIC.jpeg' + '?alt=media&token=2d7c9d30-49f4-4837-b2cb-1b3f187c77fd';
    this.images[1] = this.path + 'angularjobs.jpg' + '?alt=media&token=a43d41b6-96e1-4bed-818f-dc428dc8b5f6';
    // this.images[2] = this.path + 'politics-icon.png' + '?alt=media&token=da4542d7-8834-4986-870b-fc9978959b82';
    // this.images[3] = this.path + 'sport.JPG' + '?alt=media&token=0cf8b944-1232-44b7-9257-bf534a00f443'; 
    // this.images[4] = this.path + 'tech.JPG' + '?alt=media&token=5a19447f-4543-4878-968d-9cdc724c8487';
   }

}
