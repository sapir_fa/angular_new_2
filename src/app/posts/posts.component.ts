import { PostsService } from './../posts.service';
import { Component, OnInit } from '@angular/core';
import { Posts } from '../interfaces/posts';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.css']
})
export class PostsComponent implements OnInit {

  postsData$: Observable<Posts>;
 


  constructor(private PostsService: PostsService) { }

  ngOnInit(): void {
    this.postsData$ = this.PostsService.getPosts();
    
    
  }
}
