import { AuthService } from './../auth.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  email: string;
  password: string;
  isError: boolean = false;
  errorMessage:string;
  constructor(private Auth: AuthService, private Router: Router) { }


  onRegister() {
    this.Auth.register(this.email, this.password).then(
      res => {
        console.log('Succesful login');
        // console.log(this.afAuth.createUserWithEmailAndPassword);
        this.Router.navigateByUrl('/books');
      }).catch(
        err => {
          console.log(err);
          this.isError = true;
          this.errorMessage = err.message;
        }
      )
  }


  ngOnInit(): void {
  }

}
