import { ClassifyService } from './../classify.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-classify',
  templateUrl: './classify.component.html',
  styleUrls: ['./classify.component.css']
})
export class ClassifyComponent implements OnInit {
  [x: string]: any;

  constructor(private route:ActivatedRoute, private ClassifyService:ClassifyService) { }

  favoriteChannel: string;
  channels: string[] = ['BBC', 'CNN', 'NBC'];
  text:string;
  category:string = 'No category';
  categoryImage:string;  
  

  ngOnInit(): void {
    const temp = this.route.snapshot.params.channel;
    if(this.channels.includes(temp)){
        this.favoriteChannel = temp
    }else{
      alert("the channel is not exist")
    }
  }

  classify(){
    console.log(this.text)
     this.ClassifyService.classify(this.text).subscribe(
       res=>{console.log(res)
       this.category = this.classifyService.categories[res];
       this.categoryImage = this.imageService.images[res]; 
       }
     )
  }
}
