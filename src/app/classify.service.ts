
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ClassifyService {


  private URL:"https://rniqbt3fbh.execute-api.us-east-1.amazonaws.com/beta";
  private categories: object= {0: 'business', 1:'entertaiment', 2:'politics', 3:'sport', 4:'tech'}

classify(text:string){
  console.log(text)
  let json = {articles:[
    {text:text}
  ]}
  let body = JSON.stringify(json);
  return this.HttpClient.post<any>(this.URL, body).pipe(
    map(res=>{
      console.log(res);
      let final:string = res.body;
      console.log(final);
      final = final.replace('[',''); 
      final = final.replace(']','');
      return final; 
    })
  )
}

// classify(text:string){
//   console.log(text)
//   let json = {'articles':[
//     {'text':text}
//   ]}
//   let body = JSON.stringify(json)
//   console.log(body)
//   return this.HttpClient.post<any>(this.URL, body)
// }





// classify(text:string){
//   let json = {'articles':[
//     {'text':text}
//   ]}
//   let body = JSON.stringify(json);
//   return this.HttpClient.post<any>(this.URL, body).pipe(
//     map(res => {
//       console.log(res);
//       let final:string = res.body;
//       console.log(final);
//       final = final.replace('[',''); 
//       final = final.replace(']','');
//       return final; 
//     })
//   )

// }

  constructor( private HttpClient:HttpClient) { }



}
